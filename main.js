// sum of scores
var sum = 0
// counter to count the number of scores
var counter = 0
// get the submit button and the input
let submit_button = document.querySelector(".submit_button");
let input = document.querySelector(".score_input")
// where I'm going to put the element "score_text"
let storage = document.querySelector(".storage")

// get the calculate button
let calculate_button = document.querySelector(".calculate_button")
// where to put the text of average score (span)
let average_score_text = document.querySelector(".average_score_text")

submit_button.addEventListener("click", () => {
    let score = input.value
    console.log(score)
    if (!isNaN(score) && score <= 10 && score >= 0 && score != "") {
        score = parseFloat(score)
        counter += 1
        sum += score
        let score_text = document.createElement("span")
        score_text.innerText = `A ${counter}ª nota foi: ${score.toFixed(2)}`
        storage.append(score_text)
        input.value = ""
    }else {
        alert("Insira um número válido!")
    }
})

calculate_button.addEventListener("click", () => {
    let average_score = sum/counter
    average_score_text.innerText = `A média é: ${average_score.toFixed(2)}`

    /*Delete spans and reset counter and sum*/
    let span_elements = storage.children
    let span_list = [...span_elements]
    span_list.forEach((s) => {
        s.remove()
    })
    sum = 0
    counter = 0 
})